import java.io.*;

public class Excepcion1{

  public static void main(String args[]){
  
  BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
 
  try{
  
  System.out.println("Ingresa el denominador: ");
  int denominador = Integer.parseInt(buffer.readLine());
  
  System.out.println("Ingresa el numerador: ");
  int numerador = Integer.parseInt(buffer.readLine());

  int resultado = denominador/numerador;
  
  System.out.println("Resultado = " + resultado);
  }catch(ArithmeticException e){
  System.out.println("Error division por Cero...");
  
  }catch(IOException e){
  System.out.println("Error excepcion...");

  }catch(NumberFormatException e){
  System.out.println("Error NumberFormatException introduccion de un caracter ");

  }finally{
  System.out.println("Ejecucion del bloque finally");
  }
  
  }

}